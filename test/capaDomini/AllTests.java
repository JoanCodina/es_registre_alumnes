package capaDomini;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestRegistreBD.class, TestRegistrePantalla.class, TestRegistreFitxer.class, TestFactoria.class})
public class AllTests {

}
