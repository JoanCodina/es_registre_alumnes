package capaDomini;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestRegistreFitxer {
	
	private IRegistre registre;
	
	@BeforeClass
	public static void provar(){
		System.out.println("\n******************Provar el registre de Fitxer*********************");
	}
	
	@Before
	public void setUp() {		
		try {
			registre = new RegistreFitxerAdapter("registre.txt");
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@After
	public void Disconect() {		
		try {
			registre.tanca();
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@AfterClass
	public static  void fiTest(){
		try {
			IRegistre registre = new RegistreFitxerAdapter("registre.txt");
			List<String> registres = registre.registre();
			assertEquals("Mida registre incorrecte ", 15, registres.size());
			for (String r: registres) {
				System.out.println(r);
			}
			registre.tanca();
			registre.clear();	
			System.out.println("\n******************Fi registre de Fitxer*********************");
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testEstablirNivell() {
		try {
			registre.registra(IRegistre.Nivell.Error, "error de nivell");
			fail("permet registre sense nivell pre-establert");
		} catch (Exception e) {
			assertEquals("Permet registar sense nivell establert", "No hi ha nivell pre-establert",e.getMessage());
		}
	}

	@Test
	public void testCanviNivell() {
		try {
			String r;
			registre.estableixNivell(IRegistre.Nivell.Error);
			r=registre.registra(IRegistre.Nivell.Error, "registra Error");
			assertNotEquals("error no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Avis, "registra Avís");
			assertEquals("Avís registrat","", r);
			r=registre.registra(IRegistre.Nivell.Informacio, "registra Info");
			assertEquals("Info registrat","", r);
			r=registre.registra(IRegistre.Nivell.Debug, "registra Debug");
			assertEquals("Debug registrat","", r);
			r=registre.registra(IRegistre.Nivell.Trassa, "registra Trassa");
			assertEquals("Trassa registrat","", r);
			
			registre.estableixNivell(IRegistre.Nivell.Avis);
			r=registre.registra(IRegistre.Nivell.Error, "registra Error");
			assertNotEquals("error no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Avis, "registra Avís");
			assertNotEquals("Avís no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Informacio, "registra Info");
			assertEquals("Info registrat","", r);
			r=registre.registra(IRegistre.Nivell.Debug, "registra Debug");
			assertEquals("Debug registrat","", r);
			r=registre.registra(IRegistre.Nivell.Trassa, "registra Trassa");
			assertEquals("Trassa registrat","", r);
			
			registre.estableixNivell(IRegistre.Nivell.Informacio);
			r=registre.registra(IRegistre.Nivell.Error, "registra Error");
			assertNotEquals("error no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Avis, "registra Avís");
			assertNotEquals("Avís no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Informacio, "registra Info");
			assertNotEquals("Info no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Debug, "registra Debug");
			assertEquals("Debug registrat","", r);
			r=registre.registra(IRegistre.Nivell.Trassa, "registra Trassa");
			assertEquals("Trassa registrat","", r);
			
			registre.estableixNivell(IRegistre.Nivell.Debug);
			r=registre.registra(IRegistre.Nivell.Error, "registra Error");
			assertNotEquals("error no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Avis, "registra Avís");
			assertNotEquals("Avís no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Informacio, "registra Info");
			assertNotEquals("Info no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Debug, "registra Debug");
			assertNotEquals("Debug registrat","", r);
			r=registre.registra(IRegistre.Nivell.Trassa, "registra Trassa");
			assertEquals("Trassa registrat","", r);			

			
			registre.estableixNivell(IRegistre.Nivell.Trassa);
			r=registre.registra(IRegistre.Nivell.Error, "registra Error");
			assertNotEquals("error no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Avis, "registra Avís");
			assertNotEquals("Avís no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Informacio, "registra Info");
			assertNotEquals("Info no registrat","", r);
			r=registre.registra(IRegistre.Nivell.Debug, "registra Debug");
			assertNotEquals("Debug registrat","", r);
			r=registre.registra(IRegistre.Nivell.Trassa, "registra Trassa");
			assertNotEquals("Trassa no registrat","", r);	
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}



}
