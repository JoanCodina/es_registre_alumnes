package capaDomini;

import static org.junit.Assert.*;

import org.junit.Test;

import capaDomini.IRegistre.Nivell;

public class TestFactoria {
	
	private //TODO registre;

	@Test
	public void testFactoriaFitxer() {
		System.out.println("\n******************Provar el Fitxer*********************");
		try {
		//TODO 
		registre = FactoriaRegistres.getRegistre 
		this.testNivell();
		this.testRegistre();
		this.testTancar();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testFactoriaBBDD() {	
		System.out.println("\n******************Provar la BBDD*********************");
		try {
			//TODO 
			registre = FactoriaRegistres.getRegistre 
		this.testNivell();
		this.testRegistre();
		this.testTancar();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testFactoriaPantalla() {	
		System.out.println("\n******************Provar la Pantalla*********************");
		try {
			//TODO 
			registre = FactoriaRegistres.getRegistre 
		this.testNivell();
		this.testRegistre();
		this.testTancar();
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}


	private void testNivell() {
		try {
			registre.estableixNivell(Nivell.Debug);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}


	private void testRegistre() {
		try {
			System.out.println(this.registre.registra(Nivell.Informacio, "informem que escriu"));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}


	private void testTancar() {
		try {
			this.registre.tanca();
			System.out.println( String.join("\n",registre.registre() ));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
