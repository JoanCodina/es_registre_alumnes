package capaDomini;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class RegistreFitxer {
	static List<String> registre=null;
	static String fitxer=null;
	static boolean connectat =false;
	enum Nivell 
    { 
        ErrorGreu(4),Error(3),Avis(2),Informacio(1),Trassa(0) ;
		
        int nivell;
		   Nivell(int n) {
		      nivell = n;
		   }
		   int getNivell() {
		      return nivell;
		   }     
    } 
	
	Nivell nivell;
	List<String> preflush;
	
    public RegistreFitxer(String nom,boolean  continua, Nivell n) throws Exception {
        // obrim el fitxer
    	nivell=n;
       	preflush=new LinkedList<>();
       	if (connectat){
			throw new Exception("Encara estàs connectat"); 
		} 
       	if (continua) {
    		if (null==registre) {
    			throw new Exception("Error fitxer a continuar no creat"); 
    		} 
    		if (!nom.equals(fitxer))  {
    			throw new Exception("Error fitxer a continuar no existeix"); 
    		} 
    	}  else {
    		registre =new LinkedList<>();
    		fitxer=nom;
    	}   	
       	connectat=true;
    }


    public void flush() throws Exception {
       	if (!connectat){
			throw new Exception("No estàs connectat"); 
		} 
    	registre.addAll(preflush);
    	preflush.clear();
     }

    public String registra(Nivell nivell, String text) throws Exception {
       	if (!connectat){
			throw new Exception("No estàs connectat"); 
		} 
       	String s;
       	if (nivell.getNivell()>=this.nivell.getNivell()) {
       		s= LocalDateTime.now(Clock.systemDefaultZone()) +"\t"+nivell.toString()+"\t"+text;
       		preflush.add( s);
       		return s;
       	}
       	return "";
    }

    public String desconnectar() throws Exception {
    	flush();
    	connectat = false;
    	return("Tancant el fitxer");
    }    
    
    public static List<String> registre(){
    	return registre;
    }


	public static void clear() {
		registre.clear();
	}
}