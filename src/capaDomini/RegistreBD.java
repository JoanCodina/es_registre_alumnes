package capaDomini;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class RegistreBD {
	static List<String> registre=new LinkedList<>();;
	static boolean connectat =false;

	enum Nivell 
    { 
		Error(4),Avis(3),Informacio(2),Debug (1),Trassa(0);
		
        int nivell;
		   Nivell(int n) {
		      nivell = n;
		   }
		   int getNivell() {
		      return nivell;
		   }     
    } 
	
	Nivell nivell;

	
    public RegistreBD(String nom, String user, String passwd) throws Exception {
       	if (connectat){
			throw new Exception("Encara estàs connectat"); 
		} 
     	nivell=null;
       	connectat=true;
    }

    public void estableixNivell(Nivell niv) {
    	nivell=niv;   
    }

 
    public String registra(Nivell nivell, String text) throws Exception {
       	if (!connectat){
			throw new Exception("No estàs connectat"); 
		} 
        if (this.nivell==null) {
        	 throw new Exception("No hi ha nivell pre-establert");
        } else {
           if (nivell.getNivell()>=this.nivell.getNivell()) {
          	    String s= LocalDateTime.now(Clock.systemDefaultZone()) +"\t"+nivell.toString()+"\t"+text;
         	   registre.add( s);
         	   return s;
           }
        }
        return "";
    }

    public String desconnectar() throws Exception {
    	connectat = false;
    	return("Tancant la base de dades");
    } 
    
    public static List<String> registre(){
    	return registre;
    }

	public static  void  clear() {
		 registre.clear();
	}
}