package capaDomini;

import java.util.HashMap;
import java.util.Map;

public class Params{
	public Map<String,String> parameters;
	public static Params instance() {
		return new Params();
	}
	public Params() {
		parameters = new HashMap<>();
	}
	public Params putParam(String name, String value) {
		parameters.put(name,value);
		return this;
	}
	public boolean containsKey(String name) {
		return parameters.containsKey(name);
	}
	public String get(String name) {
		return parameters.get(name);
	}
}

