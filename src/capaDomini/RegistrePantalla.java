package capaDomini;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class RegistrePantalla {
	static List<String> registre=new LinkedList<>();

	enum Nivell 
    { 
        Error(2),Avis(1),Informacio(0) ;
		
        int nivell;
		   Nivell(int n) {
		      nivell = n;
		   }
		   int getNivell() {
		      return nivell;
		   }     
    } 
	
	Nivell nivell;
	
    public RegistrePantalla(Nivell n) throws Exception {
    	nivell=n;
    }


    public String error(String text)throws Exception {
    	return registra(Nivell.Error,text);
    }
    public String avis(String text)throws Exception {
    	return registra(Nivell.Avis,text);
    }   
    
    public String info(String text)throws Exception {
    	return registra(Nivell.Informacio,text);
    }
    
    private String registra(Nivell nivell, String text) throws Exception {
        String s="";
    	if (nivell.getNivell()>=this.nivell.getNivell()) {
           s = LocalDateTime.now(Clock.systemDefaultZone()) +"\t"+nivell.toString()+"\t"+text;
           registre.add( s);	
        }
        return s;
    }
    public static List<String> registre(){
    	return registre;
    }


	public static void  clear() {
		registre.clear();
	}
    
}